import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        ArrayList<Point> cor = new ArrayList<>();
        Scanner scan = new Scanner(new File("input.txt"));
        FileWriter fileWriter = new FileWriter("output.txt");
        while (scan.hasNext()) {
            cor.add(new Point(scan.nextDouble(), scan.nextDouble()));
        }
        double x0 = cor.get(0).getX(), y0 = cor.get(0).getY();
        cor.add(new Point(x0, y0));
        double temp;
        /*for (int i = 0; i <cor.size() ; i++) {
            temp = cor.get(i).getX();
            cor.get(i).getX() = cor.get(cor.size()-i).getX();
            cor.get(cor.size()-i).getX() = temp;
        }*/
        double s = 0, s1 = 0;
        for (int j = 0; j < cor.size() - 1; j++) {
            s = s + (cor.get(j).getX() * cor.get(j + 1).getY());
            s1 = s1 + (cor.get(j).getY() * cor.get(j + 1).getX());
        }
        DecimalFormat form = new DecimalFormat("#.##");
        form.setRoundingMode(RoundingMode.HALF_DOWN);
        form.setMinimumFractionDigits(2);

        fileWriter.write(form.format((s1 - s) / 2 * (-1)));

        scan.close();
        fileWriter.flush();
        fileWriter.close();
    }
}
